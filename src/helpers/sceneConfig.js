export default function defaultSceneConfig(scene) {

    // Définit le pointer par défault
    scene.input.setDefaultCursor('url(./assets/cursor/normal.cur), pointer');

}