/* ========================================================================== */
/*                                                                            */
/*                                 GAME SETUP                                 */
/*                                                                            */
/* ========================================================================== */

/* ----------------------------- Dev Mod Switch ----------------------------- */

export const DEVMOD = false;
export const STARTSCENE = 'Planet'; // Default : Planet

/* --------------------------------- Gravité -------------------------------- */

export const GRAVITY = 1;

/* ---------------------------- Taille du canvas ---------------------------- */

export const WIDTH = 1200;
export const HEIGHT = 600;

/* ------------------------- Origin X et Y du canvas ------------------------ */

export const ORIGIN_X = 600;
export const ORIGIN_Y = 300;

/* ---------------------------------- Typos --------------------------------- */

export const TYPO = 'Rajdhani';

/* ========================================================================== */
/*                                  GENERATOR                                 */
/* ========================================================================== */

export const NUMBER_MAX_OF_SYSTEMS = 30;
export const NUMBER_MAX_OF_PLANETS = 8;
export const NUMBER_MAX_OF_ASTEROIDS = 8;
export const ONE_PER_THIS_CHANCE_TO_HAVE_A_SAT = 2; // 1 chance on THIS
export const NUMBER_MAX_OF_MATERIALS = 8; // 12 max

/* ========================================================================== */
/*                                    SIZER                                   */
/* ========================================================================== */

export const PLANET_SIZE_FACTOR = 15;